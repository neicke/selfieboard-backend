package selfieboard

import "time"

// SelfiePost represents a selfie postet to the board
type SelfiePost struct {
	ImageURL   string    `json:"imageUrl"`
	RandomID   float64   `json:"randomId"`
	UploadTime time.Time `json:"uploadTime"`
}
