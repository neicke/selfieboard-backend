package selfieboard

import (
	"context"
	"mime/multipart"

	"cloud.google.com/go/storage"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

// StorageBucketName is the name of the bucket used for this App
var StorageBucketName = "selfieboard-backend-211009"

// StorageBucket is a handler to the storage bucket to be used for data store
var StorageBucket *storage.BucketHandle

func createStorageWriter(ctx context.Context, name string, uploadedFileHeader *multipart.FileHeader) (*storage.Writer, error) {

	if StorageBucket == nil {
		client, err := storage.NewClient(ctx)
		if err != nil {
			return nil, err
		}
		StorageBucket = client.Bucket(StorageBucketName)
	}

	// Get storage writer
	storageWriter := StorageBucket.Object(name).NewWriter(ctx)
	storageWriter.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	storageWriter.ContentType = uploadedFileHeader.Header.Get("Content-Type")

	// Entries are immutable, be aggressive about caching (1 day).
	storageWriter.CacheControl = "public, max-age=86400"

	return storageWriter, nil
}

func store(ctx context.Context, post *SelfiePost) error {
	key := datastore.NewIncompleteKey(ctx, "SelfiePost", nil)

	if _, err := datastore.Put(ctx, key, post); err != nil {
		log.Errorf(ctx, "datastore.Put: %v", err)
		return err
	}
	return nil
}
