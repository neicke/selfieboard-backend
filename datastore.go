package selfieboard

import (
	"context"
	"math/rand"
	"time"

	"google.golang.org/appengine/datastore"
)

func save(ctx context.Context, post *SelfiePost) error {

	key := datastore.NewIncompleteKey(ctx, "Post", nil)
	if _, err := datastore.Put(ctx, key, post); err != nil {
		return err
	}

	return nil
}

func getLatest(ctx context.Context) ([]SelfiePost, error) {
	q := datastore.NewQuery("Post").Order("-UploadTime").Limit(1)
	return executeQuery(ctx, q)
}

func getLatestSince(ctx context.Context, since time.Time) ([]SelfiePost, error) {
	q := datastore.NewQuery("Post").Filter("UploadTime > ", since).Order("-UploadTime")
	return executeQuery(ctx, q)
}

func getRandom(ctx context.Context) ([]SelfiePost, error) {
	random := rand.Float64()
	q := datastore.NewQuery("Post").Filter("RandomID >= ", random).Order("RandomID").Limit(1)

	result, err := executeQuery(ctx, q)
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		q = datastore.NewQuery("Post").Filter("RandomID <= ", random).Order("RandomID").Limit(1)
		return executeQuery(ctx, q)
	}

	return result, nil
}

func executeQuery(ctx context.Context, q *datastore.Query) ([]SelfiePost, error) {
	t := q.Run(ctx)

	var result = []SelfiePost{}

	for {
		var p SelfiePost
		_, err := t.Next(&p)
		if err == datastore.Done {
			break // No further entities match the query.
		}
		if err != nil {
			return nil, err
		}
		result = append(result, p)
	}

	return result, nil
}
