package selfieboard

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"google.golang.org/appengine"
)

func init() {
	r := mux.NewRouter()
	r.HandleFunc("/", homeHandler)
	r.HandleFunc("/upload", uploadHandler)
	r.HandleFunc("/latest", queryLatestHandler)
	r.HandleFunc("/since", querySinceHandler)
	r.HandleFunc("/random", queryRandomHandler)
	handler := cors.Default().Handler(r)
	http.Handle("/", handler)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
}

func queryLatestHandler(w http.ResponseWriter, r *http.Request) {

	ctx := appengine.NewContext(r)

	result, err := getLatest(ctx)

	if err != nil {
		http.Error(w, "Error uploading file: "+err.Error(), http.StatusInternalServerError)
		return
	}

	a, err := json.Marshal(result)
	fmt.Fprint(w, string(a))
}

func querySinceHandler(w http.ResponseWriter, r *http.Request) {

	sinceParameterString := r.URL.Query().Get("since")
	since, err := time.Parse(time.RFC3339Nano, sinceParameterString)
	if err != nil {
		http.Error(w, "GET-Parameter 'since' does not match ISO8601", http.StatusInternalServerError)
		return
	}

	ctx := appengine.NewContext(r)

	result, err := getLatestSince(ctx, since)

	if err != nil {
		http.Error(w, "Error uploading file: "+err.Error(), http.StatusInternalServerError)
		return
	}

	a, err := json.Marshal(result)
	fmt.Fprint(w, string(a))
}

func queryRandomHandler(w http.ResponseWriter, r *http.Request) {

	ctx := appengine.NewContext(r)

	result, err := getRandom(ctx)

	if err != nil {
		http.Error(w, "Error uploading file: "+err.Error(), http.StatusInternalServerError)
		return
	}

	a, err := json.Marshal(result)
	fmt.Fprint(w, string(a))
}
