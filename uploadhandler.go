package selfieboard

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"path"
	"time"

	"bytes"
	"cloud.google.com/go/storage"
	"github.com/disintegration/imaging"
	"github.com/satori/go.uuid"
	"google.golang.org/appengine"
)

func uploadHandler(responseWriter http.ResponseWriter, request *http.Request) {

	ctx := appengine.NewContext(request)

	// Only accept post requests
	if request.Method != http.MethodPost {
		http.Error(responseWriter, "only POST method is allowed", http.StatusForbidden)
		return
	}

	// get uploaded file
	uploadedFile, uploadedFileHeader, err := request.FormFile("usrfile")
	if err != nil {
		http.Error(responseWriter, "Error uploading file: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer uploadedFile.Close()

	// Fuer den Fall, dass etwas mit dem image decoding nicht stimmmt wird die originale Datei geklont.
	var buf bytes.Buffer
	tee := io.TeeReader(uploadedFile, &buf)

	// Sollte etwas mit dem Format nicht stimmen, wird die Orientierung ignoriert, damit das Bild trotzdem angezeigt
	// wird. Dies kann geaendert werden, indem der Fehler behandelt wird.
	img, _ := imaging.Decode(tee, imaging.AutoOrientation(true))

	// Der Buffer, in dem das encoded (hoffentlich gedrehte) Bild liegt.
	var encodedImage bytes.Buffer

	// Entweder ist das die originale Datei oder die Gedrehte.
	var readFrom = &buf

	// Es gab keinen Fehler bei der Dekodierung.
	if img != nil {
		if err := imaging.Encode(&encodedImage, img, imaging.JPEG); err == nil {
			// Wenn kein Fehler da ist wird statt der originalen Datei die Gedrehte genommen.
			readFrom = &encodedImage
		}
	}

	remoteFileName := uuid.Must(uuid.NewV4()).String() + path.Ext(uploadedFileHeader.Filename)

	client, err := storage.NewClient(ctx)
	if err != nil {
		return
	}
	defer client.Close()

	bucket := client.Bucket(StorageBucketName)
	w := bucket.Object(remoteFileName).NewWriter(ctx)
	w.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	w.CacheControl = "public, max-age=86400"

	// Wir nutzen den von uns gesetzten io.Reader.
	if _, err := io.Copy(w, readFrom); err != nil {
		http.Error(responseWriter, "Error writing file: "+err.Error(), http.StatusInternalServerError)
		return
	}
	if err := w.Close(); err != nil {
		http.Error(responseWriter, "Error closing storage writer: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// Create upload information
	randomID := rand.Float64()
	publicURL := fmt.Sprintf("https://storage.googleapis.com/%s/%s", StorageBucketName, remoteFileName)
	uploadTime := time.Now()

	post := &SelfiePost{ImageURL: publicURL, RandomID: randomID, UploadTime: uploadTime}

	if err := store(ctx, post); err != nil {
		http.Error(responseWriter, "Could not store file. Error saving file: "+err.Error(), http.StatusInternalServerError)
		return
	}

	save(ctx, post)

	b, err := json.Marshal(post)
	if err != nil {
		http.Error(responseWriter, "Error parsing json.", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	fmt.Fprint(responseWriter, string(b))
}
